﻿using Config;
using httpHelper;
using MetroFramework.Forms;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace nsCarbinet
{
    public partial class frmLogin : MetroForm
    {
        public frmLogin()
        {
            InitializeComponent();

            this.Shown += frmLogin_Shown;
        }

        void frmLogin_Shown(object sender, EventArgs e)
        {
            object host = nsConfigDB.ConfigDB.getConfig("host");
            if (host == null)
            {
                MessageBox.Show("在登录之前需要设置服务器地址！");
            }

            object comport = nsConfigDB.ConfigDB.getConfig("comport_name");
            if (comport == null)
            {
                MessageBox.Show("在登录之前需要设置网关使用的串口名称！");
            }
        }

        private void btnQuit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (txtName.Text.Length <= 0 || txtPwd.Text.Length <= 0)
            {
                return;
            }
            Regex regex = new Regex(@"(?<userID>\w+)@(?<classID>\w+)");
            if (!regex.IsMatch(txtName.Text))
            {
                MessageBox.Show("输入的用户名不符合规则！");
                return;
            }
            MatchCollection matches = regex.Matches(txtName.Text);
            Match match = matches[0];

            Program.userID = match.Groups["userID"].Value;
            Program.classID = match.Groups["classID"].Value;

            validateLoginInfo(Program.userID, getMD5(txtPwd.Text), Program.host);
            //this.Hide();
        }

        //验证登陆信息是否正确
        void validateLoginInfo(string user, string pwd, string host)
        {
            EquipmentConfigCtl.clearEquipmentMapOfDB();
            HttpWebConnect helper = new HttpWebConnect();
            helper.RequestCompleted += (o) =>
            {
                string temp = o as string;
                Debug.WriteLine(temp);
                if (temp == "ok")
                {
                    //从服务器下载数据，更新完成后打开主页面
                    Action invoke = delegate()
                    {
                        start_download_data();
                    };
                    this.Invoke(invoke);
                }
                else
                {
                    Action invoke = delegate()
                    {
                        MessageBox.Show("用户和密码不匹配，请重新输入！", "登录提示");
                    };
                    this.Invoke(invoke);
                }
            };
            string url = host + "/index.php/Login/tryLogin/user/" + user + "/pwd/" + pwd;
            helper.TryRequest(url);
        }
        private void start_download_data()
        {
            Debug.WriteLine("开始下载数据。。。");
            this.metroProgressBar1.Value = 0;
            this.metroProgressBar1.Visible = true;
            getRoomConfigFromWeb(Program.classID, Program.host);
        }
        private void stop_login(string msg)
        {
            Action invoke = delegate()
            {
                this.metroProgressBar1.Visible = false;
                MessageBox.Show(msg);
            };
            this.Invoke(invoke);
        }
        /// <summary>
        /// 设备和座位映射数据 2
        /// </summary>
        /// <param name="classID"></param>
        /// <param name="host"></param>
        public void getEquipmentMapFromWeb(string classID, string host)
        {
            EquipmentConfigCtl.clearEquipmentMapOfDB();
            HttpWebConnect helper = new HttpWebConnect();
            helper.RequestCompleted += (o) =>
            {
                string rtn = o as string;

                Debug.WriteLine(rtn);
                List<equipmentPosition> listMap = (List<equipmentPosition>)JsonConvert.DeserializeObject<List<equipmentPosition>>(rtn);

                {
                    EquipmentConfigCtl.AddMapConfig(listMap);
                    Action invoke = delegate()
                    {
                        this.metroProgressBar1.Value = 70;
                    };
                    this.Invoke(invoke);
                    getStudentInfoFromWeb(Program.classID, Program.host);
                }

            };
            string url = host + "/index.php/EquipmentMap/getEquipmentMap/class/" + classID;
            helper.TryRequest(url);
        }

        /// <summary>
        /// 教室座位配置数据  1
        /// </summary>
        /// <param name="classID"></param>
        /// <param name="host"></param>
        public void getRoomConfigFromWeb(string classID, string host)
        {
            roomConfigCtl.clearRoomConfigOfDB();
            HttpWebConnect helper = new HttpWebConnect();
            helper.RequestCompleted += (o) =>
            {
                Debug.WriteLine(o as string);
                List<RoomConfig> list = (List<RoomConfig>)JsonConvert.DeserializeObject<List<RoomConfig>>(o as string);
                roomConfigCtl.AddNewConfig(list);
                Action invoke = delegate()
                {
                    this.metroProgressBar1.Value = 30;
                };
                this.Invoke(invoke);
                getEquipmentMapFromWeb(Program.classID, Program.host);
            };
            string url = host + "/index.php/RoomConfig/getRoomConfig/class/" + classID;
            helper.TryRequest(url);
        }

        /// <summary>
        /// 获取学生信息 3
        /// </summary>
        /// <param name="classID"></param>
        /// <param name="host"></param>
        public void getStudentInfoFromWeb(string classID, string host)
        {
            studentInfoCtl.clearStudentInfo();
            HttpWebConnect helper = new HttpWebConnect();
            helper.RequestCompleted += (o) =>
            {
                string rtn = o as string;

                Debug.WriteLine(rtn);
                List<Person> personList = (List<Person>)JsonConvert.DeserializeObject<List<Person>>(rtn);
                studentInfoCtl.addStudentInfo(personList);
                if (rtn == "null" || personList == null || personList.Count <= 0)
                {
                    stop_login("学生信息下载失败，请确认登录信息中班级填写正确！");
                }
                else
                {
                    Action invoke = delegate()
                    {
                        this.metroProgressBar1.Value = 100;
                        this.Hide();
                        showMainForm();
                    };
                    this.Invoke(invoke);
                }

            };
            string url = host + "/index.php/UserManagement/user_list_for_client/class/" + classID;
            helper.TryRequest(url);
        }
        void showMainForm()
        {
            Program.sysDataConnectionInitial();
            Program.frmClassRoom = new frmClassRoom();
            Program.frmFloat = new frmFloat();
            Program.frmFloat.Show();
        }
        private string getMD5(string source)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] targetData = md5.ComputeHash(System.Text.Encoding.UTF8.GetBytes(source));
            string byte2String = "";

            for (int i = 0; i < targetData.Length; i++)
            {
                byte2String += System.Convert.ToString(targetData[i], 16).PadLeft(2, '0');
            }
            return byte2String.PadLeft(32, '0');
        }

        private void metroLink1_Click(object sender, EventArgs e)
        {
            frmConfig frm = new frmConfig();
            frm.ShowDialog();
        }
    }
}
