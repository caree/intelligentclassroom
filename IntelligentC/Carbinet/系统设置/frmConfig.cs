﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Config;
using System.IO.Ports;
using MetroFramework.Forms;
using System.Text.RegularExpressions;
using nsCarbinet;

namespace Config
{
    public partial class frmConfig : MetroForm
    {
        public frmConfig()
        {
            InitializeComponent();

            this.LoadConfig();

            string[] ports = SerialPort.GetPortNames();
            Array.Sort(ports);
            cmbPortName.Items.AddRange(ports);
        }

        void LoadConfig()
        {
            object host = nsConfigDB.ConfigDB.getConfig("host");
            if (host != null)
            {
                this.textBox1.Text = host as string;
            }

            object comport = nsConfigDB.ConfigDB.getConfig("comport_name");
            if (comport != null)
            {
                cmbPortName.Text = comport as string;
            }
        }

        private void btnSaveConfig_Click(object sender, EventArgs e)
        {
            string pattern = @"http://\b(([01]?\d?\d|2[0-4]\d|25[0-5])\.){3}([01]?\d?\d|2[0-4]\d|25[0-5])\b:[0-9]{1,5}";
            string input = this.textBox1.Text;
            if (input.Length <= 0 || !Regex.IsMatch(input, pattern))
            {
                MessageBox.Show("请填写一个符合要求的地址！");
                return;
            }
            MatchCollection matches = Regex.Matches(input, pattern);
            Match match = matches[0];
            string host = match.Groups[0].Value;
            nsConfigDB.ConfigDB.saveConfig("host", host);
            nsConfigDB.ConfigDB.saveConfig("comport_name", cmbPortName.Text);

            Program.host = host;
            Program.comport_name = cmbPortName.Text;

            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();

        }
    }
}
